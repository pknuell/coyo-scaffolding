(function () {
  'use strict';

  function EventDetails() {
    var api = this;

    api.title = $('.title');

    api.options = {
      settings: $('.content-sidebar a[ui-sref="main.event.show.settings"]')
    };

    api.settings = {
      deleteButton: $('a[ng-click="$ctrl.deleteEvent()"]')
    };
  }

  module.exports = EventDetails;
})();
