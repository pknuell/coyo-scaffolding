(function () {
  'use strict';

  var extend = require('util')._extend;

  function TeaserWidget(widget) {
    var api = extend(this, widget);

    api.openSettings = function (slideIndex) {
      element.all(by.className('teaser-slide-context')).get(slideIndex).click();
      element.all(by.className('zmdi-edit')).get(slideIndex).click();
    };

    api.setUrl = function (url) {
      element(by.model('$ctrl.model.config._url')).clear().sendKeys(url);
    };

    api.saveSettings = function () {
      $('.modal-body .btn-primary[type="submit"]').click();
    };

    api.clickSlide = function (slideIndex) {
      element.all(by.css('.swiper-pagination-clickable .swiper-pagination-bullet')).get(slideIndex).click();
      element(by.className('swiper-slide-active')).click();
    };
  }

  module.exports = TeaserWidget;
})();
